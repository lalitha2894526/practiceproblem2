const data = require('../data.json');
const findVideoGameUsers = require('../problem1.js');
const users = data[0];

const videoGameUsers = findVideoGameUsers(users);
console.log("The users who are interested in playing video games are:\n");
console.log(videoGameUsers);
