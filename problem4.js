function groupUsersByLanguage(users) {
    try {
        const groupedUsers = {};

        for (let username in users) {
            const designationParts = users[username].designation.split(" ");
            const language = designationParts[0].toLowerCase();

            if (!groupedUsers[language]) {
                groupedUsers[language] = [];
            }

            groupedUsers[language].push(username);
        }

        return groupedUsers;
    } 
    catch (error) {
        console.error('An error occurred:', error.message);
        return {}; 
    }
}

module.exports = groupUsersByLanguage;
