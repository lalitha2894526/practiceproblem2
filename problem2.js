function findUsersInGermany(users) {
    try {
        const usersInGermany = [];
        for (let username in users) {
            if (users[username].nationality === "Germany") {
                usersInGermany.push(username);
            }
        }
        return usersInGermany;
    }
     catch (error) {
        console.error('An error occurred:', error.message);
        return []; 
    }
}

module.exports = findUsersInGermany;
