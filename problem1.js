function findVideoGameUsers(users) {
    try {
        const videoGameUsers = [];
        for (let username in users) {
            if (users[username].interests && users[username].interests.includes("Playing Video Games")) {
                videoGameUsers.push(username);
            }
        }
        return videoGameUsers;
    }
     catch (error) {
        console.error('An error occurred:', error.message);
        return [];
    }
}

module.exports = findVideoGameUsers;
