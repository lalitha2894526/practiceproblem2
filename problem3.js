function findUsersWithMasters(users) {
    try {
        const usersWithMasters = [];
        for (let username in users) {
            if (users[username].qualification === "Masters") {
                usersWithMasters.push(username);
            }
        }
        return usersWithMasters;
    }
     catch (error) {
        console.error('An error occurred:', error.message);
        return [];
    }
}

module.exports = findUsersWithMasters;
